#include "..\api\util\util.hpp"
#include "..\api\game_reader\game_reader.hpp"
#include "..\api\ini_reader\ini_reader.hpp"
#include "..\api\common.h"

#include <regex>

int main( ) {
	//	construct our file
	const auto config = std::make_unique< ini_reader >( utility::current_directory( ), "config.ini" );

	// initalize needed stuff
	std::vector<std::string> game_files;
	std::vector<std::string> game_files_nobz2;
	std::vector<std::string> game_files_rsc;

	printf( "looking for config..." );
	{
		//	ensure file exists
		if ( !config->exists( ) ) {
			std::stringstream temp;
			temp << utility::current_directory( ) << "\\" << "config.ini";

			std::string _file_path = temp.str( );
			printf( "current_directory_file: %s.\n", _file_path.c_str( ) );
			printf( "current_directory: %s.\n", utility::current_directory( ).c_str( ) );
			printf( "failed to find config." );

			system( "pause" );
			return 0;
		}

		printf( "config found.\n" );
	}

	printf( "dumping contents..." );
	{
		config->dump( );
		printf( "dump complete.\n" );
	}

	const auto default_game_files = std::make_unique< game_reader >( utility::file_single( utility::current_directory( ), config->value( "game" ) ), config->value( "game_dir" ), config->value( "game" ) );

	printf( "listing variables..." );
	{
		config->read( );
		printf( "variables have been read.\n" );
	}

	printf( "searching for game base file...\n" );
	{
		// sanity check shouldn't happen.
		if ( default_game_files->empty( ) ) {
			printf( "game file doesn't exist or have anything in it." );

			system( "pause" );
			return 0;
		}

		printf( "game file found: %s\n", config->value( "game" ).c_str( ) );

		printf( "querying compressed fastdl / server files...\n" );
		{
			int total_files = 0, extra_files = 0;
			for ( auto& direntry : std::filesystem::recursive_directory_iterator( config->value( "game_dir" ) ) ) {
				std::string directory_string = direntry.path( ).relative_path( ).generic_string( );
				const std::string game_file = directory_string.erase( 0, directory_string.find( config->value( "game_dir" ) ) + config->value( "game_dir" ).size( ) - 1 );

				if ( is_directory( direntry ) ) {
					continue;
				}

				total_files++;

				std::stringstream ss( config->value( "exclude_ext" ) );
				const std::istream_iterator<std::string> begin( ss );
				const std::istream_iterator<std::string> end;
				const std::vector<std::string> list_exc( begin, end );

				for ( auto& exclude : list_exc ) {
					if ( directory_string.find( exclude ) != std::string::npos ) {
						goto start;
					}
				}

				for ( auto& gamefile : default_game_files->contents( ) ) {
					std::stringstream temp_stream;
					temp_stream << config->value( "game_dir" ) << "/" << gamefile;
					if ( game_file.find( gamefile ) != std::string::npos ) {
						goto start;
					}
				}

				game_files.push_back( game_file + ".bz2" );
				game_files_nobz2.push_back( game_file );
				extra_files++;

			start:
				continue;
			}

			printf( "files found on server: %i", total_files );
			printf( "total extra files: %i\n", extra_files );
			printf( "querying completed.\n" );

			printf( "querying redirection folder...\n" );
			{
				int total_redirection_files = 0;
				for ( auto& direntry : std::filesystem::recursive_directory_iterator( config->value( "redirection_dir" ) ) ) {
					std::string directory_string = direntry.path( ).relative_path( ).generic_string( );

					if ( is_directory( direntry ) ) {
						continue;
					}

					total_redirection_files++;

					for ( int i = 0; i < game_files.size( ); i++ ) {
						std::string file = game_files [ i ];
						std::stringstream temp_stream;
						temp_stream << config->value( "redirection_dir" ) << "/" << file;
						if ( directory_string.find( file ) != std::string::npos ) {
							game_files.erase( game_files.begin( ) + i );
							goto start_2;
						}
					}
				start_2:
					continue;
				}

				printf( "files found on redirect: %i\n", total_redirection_files );
				printf( "total files missing on redirect: %i\n", game_files.size( ) );
				printf( "querying completed.\n" );
			}
		}
	}

	printf( "compressing files.\n" );
	{
		for (auto& file : game_files ) {
			std::stringstream temp_stream;
			std::string final_file = std::regex_replace( file, std::regex( "(.bz2)+" ), "" );
			temp_stream << "7z.exe a -tbzip2 " << config->value( "redirection_dir" ) << "/" << file << " " << config->value( "game_dir" ) << "/" << final_file;
			system( temp_stream.str( ).c_str( ) );
		}
	}

	default_game_files->store( game_files_nobz2 );

	printf( "process has been completed." );
	{
		if ( atoi( config->value( "stop_done" ).c_str( ) ) ) {
			system( "pause" );
		}
	}
	return 0;
}