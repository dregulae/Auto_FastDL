/*
 *	common.h
 *	  - common inclusions
 */

#pragma once

#include <sys\stat.h>
#include <Windows.h>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <filesystem>

inline void printpf( const char *input, ... ) {
	printf( "[sharp parse] " );

	va_list args;
	va_start( args, input );
	vprintf( input, args );
	va_end( args );

	printf( "\n" );
}
#define printf printpf