/*
 *	game_reader.hpp
 *	  - reads game and works according to what it is
 */

#pragma once
#include <sstream>
#include <string>
#include <vector>
#include <array>

enum game_types {
	css,
	csgo,
	dods,
	gmod,
	hl2dm,
	l4d,
	tf2,
	custom
};

class game_reader {
	std::string _game_name { };
	std::string _game_dir { };
	std::string _file_path { };
	std::array < std::string, custom > game_types_strings { "css", "csgo", "dods", "gmod", "hl2dm", "l4d", "tf2" };

	game_types game_type( std::string ) const;

public:
	std::vector < std::string > _file_contents;

	game_reader( const std::vector < std::string > &file_contents, const std::string &game_dir, const std::string &game_name ) : _file_contents( file_contents ), _game_dir( game_dir ), _game_name( game_name ) {
		std::stringstream temp_stream;

		game_types game = game_type( _game_name );

		if ( game == gmod ) {
			temp_stream << _game_dir << R"(\lua\autorun\server\resources.lua)";

			_file_path = temp_stream.str();
			printf( "%s\n", _file_path.c_str( ) );
		}
	}

	std::vector < std::string > contents( ) const { return _file_contents; }

	bool empty( ) const;
	void dump( );

	void store( std::vector < std::string > );
};
