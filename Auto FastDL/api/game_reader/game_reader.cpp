#include "game_reader.hpp"
#include "..\common.h"
#include <fstream>
#include "..\ini_reader\ini_reader.hpp"

/*
*	game_type
*	  - returns enum of game type and what to do
*/
game_types game_reader::game_type( std::string line ) const {
	game_types type = game_types::custom;

	for ( int i = 0; i < game_types::custom; i++ ) {
		if ( line.find( game_types_strings [ i ] ) != std::string::npos ) {
			type = game_types( i );
		}
	}

	return type;
}

/*
*	empty
*	  - returns whether input file is empty
*/
bool game_reader::empty( ) const {
	return _file_contents.empty( );
}

/*
*	dump
*	  - fills our vector with ever variable and their values
*/
void game_reader::dump( ) {
	std::ifstream input_stream { _game_dir.c_str( ) };
	std::string line;

	//	read file
	while ( std::getline( input_stream, line ) ) {
		//	line has a variable and is not a comment
		if ( line.empty( ) ) {
			continue;
		}

		_file_contents.push_back( line );
	}
}

/*
*	dump
*	  - fills our vector with ever variable and their values
*/
void game_reader::store( std::vector < std::string > content ) {
	game_types game = game_type( _game_name );

	if ( game != gmod ) {
		return;
	}
	printf( "checking resources.lua file" );
	{
		std::vector < std::string > temp_contents;
		std::ofstream input_stream { _file_path.c_str( ) };

		if ( !input_stream.is_open( ) ) {
			return;
		}

		//	input data into file
		for ( auto& line : content ) {
			printf( "%s\n", line.c_str( ) );
			std::stringstream temp_content;
			temp_content << R"(resource.AddFile( ")" << line << R"(" ))" << "\n";
			input_stream << temp_content.str( ).c_str( );
		}

		input_stream.close( );
	}
}