#pragma once

#include <string>
#include <vector>

class utility {
public:
	static std::string current_directory( );
	static std::string file_all( std::string, std::string );
	static std::vector<std::string> file_single( std::string, std::string );
};