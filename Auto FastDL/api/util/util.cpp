#include "util.hpp"
#include "..\common.h"

#include <fstream>
#include <sstream>

#include <Shlwapi.h>
#include <iterator>
#pragma comment(lib ,"shlwapi.lib")

/*
 *	get_current_directory
 *	  - returns the directory the application was ran from.
 */
std::string utility::current_directory( ) {
	return std::filesystem::current_path( ).generic_string( );
}

std::string utility::file_all( std::string directory, std::string file_name ) {
	std::stringstream temp;
	temp << directory << "\\" << file_name;

	std::ifstream temp_file( temp.str( ) );
	return std::string( ( std::istreambuf_iterator<char>( temp_file ) ), std::istreambuf_iterator<char>( ) );
}



std::vector<std::string> utility::file_single( std::string directory, std::string file_name ) {
	std::stringstream temp;
	std::vector<std::string> temp_deque;
	temp << directory << "\\" << file_name;

	std::ifstream temp_file( temp.str( ) );
	std::string contents = std::string( ( std::istreambuf_iterator<char>( temp_file ) ), std::istreambuf_iterator<char>( ) );
	std::stringstream contents_stream( contents );
	const std::istream_iterator<std::string> begin( contents_stream );
	const std::istream_iterator <std::string> end;
	return std::vector<std::string>( begin, end );
}
