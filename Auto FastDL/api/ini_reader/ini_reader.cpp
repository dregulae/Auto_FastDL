#include "ini_reader.hpp"
#include "..\common.h"
#include <fstream>

/*
*	comment
*	  - returns enum of comment type and what to do
*/
comments ini_reader::comment( std::string line ) const {
	comments type = comments::nothing;

	if ( line [ 0 ] == '/' && line [ 1 ] == '/' ) {
		type = comments::regular;
	}
	else if ( line.find( "//" ) != std::string::npos ) {
		type = comments::side;
	}

	return type;
}


/*
 *	exists
 *	  - returns whether input file exists
 */
bool ini_reader::exists( ) const {
	return std::filesystem::exists( _file_path );
}

/*
 *	dump
 *	  - fills our vector with ever variable and their values
 */
void ini_reader::dump( ) {
	std::ifstream input_stream { _file_path.c_str( ) };
	std::string line;

	//	read file
	while ( std::getline( input_stream, line ) ) {
		if ( comment( line ) == comments::regular ) {
			continue;
		}
		else if ( comment( line ) == comments::side ) {
			// check if has space before comment or not then act accordingly
			const std::size_t supposed_comment = line.find_first_of( '/' );

			if ( line [ supposed_comment - 1 ] == ' ' ) {
				line = line.substr( 0, line.find( " //" ) );
			}
			else {
				line = line.substr( 0, line.find( "//" ) );
			}
		}

		//	line has a variable and is not a comment
		if ( line.empty( ) ) {
			continue;
		}

		std::pair< std::string, std::string > variable;
		variable.first = line.substr( 0, line.find( " =" ) );
		variable.second = line.substr( line.find( '=' ) + 2 );

		_file_contents.push_back( variable );
	}
}

/*
 *	read
 *	  - prints every variable
 */
void ini_reader::read( ) {
	//	sanity, should never occur
	if ( _file_contents.empty( ) ) {
		return;
	}

	//	print ever variable and their respective value 
	for ( auto var : _file_contents ) {
		printf( "%s = %s", var.first.c_str( ), var.second.c_str( ) );
	}
}

/*
 *	read
 *	  - prints a specific variable and it's respective value
 */
void ini_reader::read( const std::string &variable ) {
	//	sanity, should never occur
	if ( _file_contents.empty( ) ) {
		return;
	}

	//	iterate our variables
	for ( auto var : _file_contents ) {
		if ( var.first.find( variable ) != std::string::npos ) {
			printf( "%s = %s", var.first.c_str( ), var.second.c_str( ) );
			break;
		}
	}
}

/*
*	value
*	  - gets value of specific variable and then can convert to any type later on
*/
std::string ini_reader::value( const std::string &variable ) const {
	//	sanity, should never occur
	if ( _file_contents.empty( ) ) {
		return "wtf noding here [_file_contents.empty()]";
	}

	//	iterate our variables
	for ( auto var : _file_contents ) {
		if ( var.first.find( variable ) != std::string::npos ) {
			return var.second;
		}
	}

	return "wtf nothing with that variable name here [_file_contents]";
}